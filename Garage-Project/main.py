from garage import *
from vehicle import *

garage1 = Garage()

# List all vehicles in the garage (should be empty to begin with)
garage1.list_all_vehicles()
print("\n")

# Add vehicle details to the garage
car1 = Car("Ford", "Focus", "NA71 FOR", 5, "Top", 50000)
garage1.add_vehicle(car1)
car2 = Car("SEAT", "Alteca", "NA68 SEA", 5, "Mid", 20000)
garage1.add_vehicle(car2)
car3 = Car("Audi", "A5", 5, "NA67 AUD", "Basic", 50000)
garage1.add_vehicle(car3)
van1 = Van("Ford", "Transit", "NA59 FOR", "SWB", 5.5, False)
garage1.add_vehicle(van1)
van2 = Van("Mercedes", "Sprinter", "NA58 MER", "LWB", 9.5, False)
garage1.add_vehicle(van2)
van3 = Van("Ford", "Transit", "LWB", "NA66 FOR", 9.5, True)
garage1.add_vehicle(van3)
bus1 = Bus("Dennis", "Enviro200", "NA68 DEN", False, 30, True)
garage1.add_vehicle(bus1)
bus2 = Bus("Dennis", "Enviro400", "NA66 DEN", True, 70, True)
garage1.add_vehicle(bus2)
bus3 = Bus("Leyland", "Olimpian", "NA54 LEY", True, 60, False)
garage1.add_vehicle(bus3)

# List all vehicles in the garage (should be populated with details above)
garage1.list_all_vehicles()
print("\n")

# Remove vehicle details from the garage
garage1.remove_vehicle(bus3)

# Replace vehicle details in the garage

# Provide a vehicle fix quote - for a damaged van
garage1.fix_quote(5)
# Provide a vehicle fix quote - for a double-decker bus
garage1.fix_quote(7)

# Fix a single vehicle in the garage
garage1.repair_vehicle(0)
garage1.list_all_vehicles()
print("\n")

# Fix all vehicles in the garage
garage1.fix_all_vehicles(0)
