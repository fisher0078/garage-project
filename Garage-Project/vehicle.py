from abc import abstractmethod

class Vehicle:
    @abstractmethod
    def fixVehicle():
        pass

    def __init__(self, make, model, reg):
        self.make = make
        self.model = model
        self.reg = reg

class Car(Vehicle):
    def __init__(self, make, model, reg, doors, trim, milage):
        super().__init__(make, model, reg)
        self.doors = doors
        self.trim = trim
        self.milage = milage

    def fixVehicle(self):
        cost = 100.0
        if self.milage <= 5000:
            cost = cost * 1.2
        elif self.milage <= 10000:
            cost = cost * 1.4
        elif self.milage <= 20000:
            cost = cost * 1.6
        elif self.milage <= 60000:
            cost = cost * 2.0
        else:
            cost = cost * 3.0
        return cost

class Van(Vehicle):
    def __init__(self, make, model, reg, wheelbase, capacity, damaged):
        super().__init__(make, model, reg)
        self.wheelbase = wheelbase
        self.capacity = capacity
        self.damaged = damaged

    def fixVehicle(self):
        cost = 100.0
        if self.damaged == True:
            cost = cost * 5
        else:
            cost = cost * 1.3
        return cost

class Bus(Vehicle):
    def __init__(self, make, model, reg, double_decker, seats, electric):
        super().__init__(make, model, reg)
        self.double_decker = double_decker
        self.seats = seats
        self.electric = electric

    def fixVehicle(self):
        cost = self.seats * 10
        if self.double_decker == True:
            cost = cost * 2
        return cost
