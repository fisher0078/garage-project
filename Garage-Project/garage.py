from vehicle import *

class Garage():
    vehicleList = []
    def list_all_vehicles(self):
            print(self.vehicleList)
            for vehicle in self.vehicleList:
                print(vehicle)
                attributes = vars(vehicle)
                print(attributes)
            return()

    def add_vehicle(self, vehicle):
        self.vehicleList.append(vehicle)

    def remove_vehicle(self, vehicle):
        reg1 = getattr(vehicle, 'reg')
        for vehicle in self.vehicleList:
            attributes = vars(vehicle)
            if attributes["reg"] == reg1:
                self.vehicleList.remove(vehicle)
                print(f"Vehicle has been removed from the garage: {attributes}\n")

    def replace_vehicle(self, vehicle):
        pass

    def fix_quote(self, vehicleIndex):
        cost = self.vehicleList[vehicleIndex].fixVehicle()
        attributes = vars(self.vehicleList[vehicleIndex])
        print(f"Quote to fix vehicle costs: {cost}\n{attributes}\n")

    def repair_vehicle(self, vehicleIndex):
        cost = self.vehicleList[vehicleIndex].fixVehicle()
        attributes = vars(self.vehicleList[vehicleIndex])
        print(f"Vehicle has been repaired and the cost is: {cost}\n{attributes}\n")
        self.remove_vehicle(self.vehicleList[vehicleIndex])

    def fix_all_vehicles(self, vehicleIndex):
        total = 0
        for vehicle in self.vehicleList:
            cost = self.vehicleList[vehicleIndex].fixVehicle()
            attributes = vars(self.vehicleList[vehicleIndex])
            print(f"Vehicle has been repaired and the cost is: {cost}\n{attributes}\n")
            self.remove_vehicle(self.vehicleList[vehicleIndex])
            vehicleIndex += 1
            total += cost
        
        print(f"The total vehicle repair cost is: {total}\n")        
    